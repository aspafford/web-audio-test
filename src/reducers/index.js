const INITIAL_STATE = {
}

export const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_AUDIO_CONTEXT": {
      return {...state, audioCtx: action.payload}
    }
    case "SET_MASTER_GAIN": {
      return {...state, masterGain: action.payload}
    }
    default: 
      return state
  }
}

export default reducer