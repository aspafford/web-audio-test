function Sine(context) {
  this.context = context
  this.osc = this.context.createOscillator()
}

Sine.prototype.start = function(time) {
  this.osc.frequency.setValueAtTime(80, time);
  this.osc.start(time);
}

Sine.prototype.stop = function(time) {
  this.osc.stop(time)
}

module.exports = Sine