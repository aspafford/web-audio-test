// synths
import Sine from '../synth/Sine'

// reducers
const setAudioContext = payload => ({
  type: 'SET_AUDIO_CONTEXT',
  payload: payload
})
const setMasterGain = payload => ({
  type: 'SET_MASTER_GAIN',
  payload: payload
})

export const initSynth = () => {
  return (dispatch, getState) => {

    // set audio context
    // to-do: catch false condition
    const audioCtx = new (window.AudioContext || window.webkitAudioContext || false)();
    dispatch(setAudioContext(audioCtx))

    const time = audioCtx.currentTime

    // set master gain
    const gain = audioCtx.createGain()
    gain.gain.value = 0.5
    gain.connect(audioCtx.destination)
    dispatch(setMasterGain(gain))

    // start test sine
    const sine = new Sine(audioCtx)
    sine.osc.connect(gain)
    sine.start(audioCtx.currentTime)
    sine.stop(audioCtx.currentTime + 5)
  }
}