import React, { Component } from 'react';
import { applyMiddleware, compose, createStore } from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import logger from 'redux-logger'
// reducers
import reducer from './reducers/index.js'
// actions
import { initSynth } from './actions/initSynth'

import './App.css';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(reducer, /* preloadedState, */ composeEnhancers(
  applyMiddleware(thunk, logger)
));

class App extends Component {

  componentDidMount() {
    store.dispatch(initSynth())
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          here
        </div>
      </Provider>
    );
  }
}

export default App;
